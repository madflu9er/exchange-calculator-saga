import createSagaMiddleware from 'redux-saga'
import React from "react";
import {render} from "react-dom";
import App from "./app";
import "./styles/index.scss";
import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer } from "./redux-store/reducers/rootReducer";
import { Provider } from "react-redux";
import rootSaga from './redux-store/saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, compose(applyMiddleware(sagaMiddleware),
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));
sagaMiddleware.run(rootSaga)

const app = (
    <Provider store={store}>
        <App />
    </Provider>
)

render(app, document.getElementById("root"));
