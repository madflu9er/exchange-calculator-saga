const API_URL = "https://min-api.cryptocompare.com/data/pricemulti";

export default {
    getMuliCourseURL: (fsyms: Array<string>, tsyms:Array<string>): string => `${API_URL}?fsyms=${fsyms.toString()}&tsyms=${tsyms.toString()}`
}