import axios from "axios";
import URI from "./url";
import { objectToArray } from "base/obj-extension";

import {fsyms, tsfms} from "data/local-data";
import { ICoin } from "components/CoinBlock/types";
import { ITyped } from "base/types-utils";

export async function getCoinsData (): Promise<ICoin []> {
    const url = URI.getMuliCourseURL(fsyms, tsfms);
    const response = await axios.get(url)
    const currencyArray: ICoin [] = objectToArray(response.data, (cName: string, cData: ITyped<number>) => ({name: cName, data: cData}));
    return currencyArray
}