import React from "react";
import ExchangeCalculator from "components/ExchangeCalculator/ExchangeCalculator"

const App = () => {
    return (
        <ExchangeCalculator />
    )
};

export default App;