import { SHOW_LOADER, HIDE_LOADER } from "../types";
import { IAction } from "base/types-utils";

export const showLoader = (): IAction =>
    ({type: SHOW_LOADER});

export const hideLoader = (): IAction =>
    ({type: HIDE_LOADER});
