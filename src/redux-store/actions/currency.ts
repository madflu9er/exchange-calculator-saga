import { SET_CURRENT_CURRENCY } from "../types";
import { IAction } from "base/types-utils";

export const setCurrency = (currency): IAction =>
    ({type: SET_CURRENT_CURRENCY, payload: currency});
