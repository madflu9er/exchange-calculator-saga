import { IAction } from "base/types-utils"
import { GET_COINS_DATA, SET_COINTS_DATA, SET_CURRENT_COIN } from "../types"

export const getCoinsData = (): IAction =>
    ({type: GET_COINS_DATA});

    export const setCoinsData = (payload): IAction =>
    ({type: SET_COINTS_DATA, payload})

export const setCurrentCoin = (coin) =>
    ({type: SET_CURRENT_COIN, payload: coin})