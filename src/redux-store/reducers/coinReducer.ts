import { IAction } from "base/types-utils";
import { GET_COINS_DATA, SET_COINTS_DATA, SET_CURRENT_COIN } from "../types";

const initialState = {
    coinsData: [],
    currentCoin: null
};

export const coinsReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case GET_COINS_DATA:
            return {...state}
        case SET_COINTS_DATA :
            return {...state, coinsData: action.payload, currentCoin: action.payload[0]}
        case SET_CURRENT_COIN:
            return {...state, currentCoin: action.payload}
        default:
            return state;
    }
};
