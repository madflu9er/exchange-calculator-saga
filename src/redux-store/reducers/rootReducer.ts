import { combineReducers } from "redux";
import { coinsReducer } from "./coinReducer";
import { currencyReducer } from "./currencyReducer";
import { loaderReducer } from "./loaderReducer";

export const rootReducer = combineReducers({
    coins: coinsReducer,
    currency: currencyReducer,
    loader: loaderReducer,
});
