import {tsfms} from "data/local-data";
import { IAction } from "base/types-utils";
import { SET_CURRENT_CURRENCY } from "../types";

const initialState = {
    currentCurrency: tsfms[0]
}

export const currencyReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case SET_CURRENT_CURRENCY:
            return {...state, currentCurrency: action.payload}
        default:
            return state
    }
}
