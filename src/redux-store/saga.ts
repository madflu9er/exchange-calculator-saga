import '@babel/polyfill';
import {takeEvery, put, call, all} from "redux-saga/effects";
import { GET_COINS_DATA, SET_COINTS_DATA } from "./types";
import { getCoinsData } from "api/currency";
import { setCoinsData } from './actions/coins';
import { showLoader, hideLoader } from './actions/loader';

export default function* rootSaga() {
    yield all([
        watchGetCoinsDataAsync(),
    ])
  }

function* watchGetCoinsDataAsync() {
    yield takeEvery(GET_COINS_DATA, getCoinsDataAsync)
}

function* getCoinsDataAsync() {
    yield put(showLoader());
    const payload = yield call(getCoinsData);
    yield put(setCoinsData(payload));
    yield put(hideLoader());
}