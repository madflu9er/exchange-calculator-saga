import React from "react";
import { IExchangeOutput } from "./types";

const ExchangeOutput = ({volume, coinName, amount, currencyName}: IExchangeOutput) => {
    const roundedAmount = Number(amount).toFixed(2);
    return (
        <span>
            <span className="bold">{`${volume}${coinName} `}</span>
            will be
            <span className="bold">{` ${roundedAmount} `}</span>
            in
            <span className="bold">{` ${currencyName}`}</span>
        </span>
    )
}

export default ExchangeOutput;
