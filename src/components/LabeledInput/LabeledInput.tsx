import React from "react";
import { ILabeledInput } from "./types";

const LabeledInput = ({label, value, readonly = false, className = "", onChange, type}: ILabeledInput) => {
    const changeInput = (e) => {
        if (readonly)
            return;
        const volume = e.target.value
        onChange(volume);
    }

    return (
        <div className={`labeled-input ${className}`}>
            <span>{label}: </span>
            { readonly
                ? (<span>{value}</span>)
                : (<input type={type} value={value} onChange={changeInput}/>)
            }
        </div>
    )
}

export default LabeledInput;
