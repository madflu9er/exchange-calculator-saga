import React, {useEffect, useState} from "react";
import LabeledInput from "components/LabeledInput/LabeledInput";
import ExchangeOutput from "components/ExchangeOutput/ExchangeOutput";
import CurrencySelector from "components/CurrencySelector/CurrencySelector";
import { FieldTypeEnum } from "base/types-utils";
import Coins from "components/Coins/Coins";
import { tsfms } from "data/local-data";
import { connect } from "react-redux";
import { getCoinsData } from "../../redux-store/actions/coins";
import { setCurrency } from "../../redux-store/actions/currency";

const ExchangeCalculator = ({loadCoinsData, currentCoin, currentCurrency, setCurrentCurrency}) => {
    const [result, setResult] = useState(0);
    const [volume, setVolume] = useState(0);

    useEffect(() => {
        if (volume && currentCoin && currentCurrency) {
            const currencyValue = currentCoin.data[currentCurrency];
            const res = currencyValue * volume
            setResult(res);
        }

    }, [volume, currentCoin, currentCurrency])

    useEffect(() => {
        loadCoinsData();
    }, []);

    return (
        <div className="container">
            <div className="coin-block-wrapper">
                <Coins />
            </div>
            {currentCurrency && currentCoin &&
                <LabeledInput
                    label={"Selected coin"}
                    readonly
                    className={"output"}
                    value={currentCoin.name}
                    type={FieldTypeEnum.string}
                />
            }
            <LabeledInput
                label={"Volume"}
                onChange={setVolume}
                value={volume}
                type={FieldTypeEnum.number}
            />
            {currentCurrency &&
                <CurrencySelector
                    currencyList={tsfms}
                    onSelect={setCurrentCurrency}
                    current={currentCurrency}
                />
            }
            {currentCoin &&
                <ExchangeOutput
                    volume={volume}
                    coinName={currentCoin.name}
                    currencyName={currentCurrency}
                    amount={result}
                />
            }
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        loadCoinsData: () => dispatch(getCoinsData()),
        setCurrentCurrency: (currency) => dispatch(setCurrency(currency))
    }
}

const mapStateToProps = state => {
    return ({
        currentCoin: state.coins.currentCoin,
        currentCurrency: state.currency.currentCurrency,
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeCalculator);