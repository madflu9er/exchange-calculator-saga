import { ITyped, IAction } from "base/types-utils";

export interface ICoin {
    name: string;
    data: ITyped<number>;
    setCurrentCoin?: (data) => IAction
}