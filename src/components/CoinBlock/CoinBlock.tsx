import React from "react";
import { ICoin } from "./types";
import { connect } from "react-redux";
import { getImg } from "data/currency-img";
import { objectToArray } from "base/obj-extension";
import CourseString from "components/CourseString/CourseString";
import { setCurrentCoin } from "../../redux-store/actions/coins";


const CoinBlock = ({ name, data, setCurrentCoin }: ICoin) => {

    const imgSrc = getImg(name);
    const dataAsArray = objectToArray(data, (cName, value) => ({name:cName, value}));

    return (
        <div className="coin-block" onClick={() => setCurrentCoin({name, data})}>
            <div className="coin-img">
                <img src={imgSrc} alt={name} />
                <div className="name">{name}</div>
            </div>
            <div className="coin-data">
                {dataAsArray && dataAsArray.map(({name, value}) => (
                    <CourseString name={name} value={value} key={name+value}/>
                ))}
            </div>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return ({
        setCurrentCoin: (coin) => dispatch(setCurrentCoin(coin))
    })
}

export default connect(null, mapDispatchToProps)(CoinBlock);