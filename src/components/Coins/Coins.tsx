import React from "react";
import { connect } from "react-redux";
import CoinBlock from "components/CoinBlock/CoinBlock";
import Loader from "components/Loader/Loader";

const Coins = ({ coinsData, loading }) => {
    if (loading)
        return (<Loader />);

        return (
            <>
                {coinsData && coinsData.map(({name, data}) => (
                    <CoinBlock name={name} data={data} key={name}/>
                ))}
            </>
        )
}

const mapStateToProps = state => {
    return {
        coinsData: state.coins.coinsData,
        loading: state.loader.loading,
    };
}

export default connect(mapStateToProps)(Coins);